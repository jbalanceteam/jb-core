package org.jbalance.core.model;

import java.io.Serializable;

import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import org.jbalance.core.model.security.Oper;

import java.util.Date;
import javax.persistence.FetchType;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import org.jbalance.core.utils.Obj;
import org.jbalance.core.utils.UUIDGenerator;

@MappedSuperclass
@XmlAccessorType(XmlAccessType.FIELD)
public class BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "id", updatable = false, nullable = false)
    protected Long id;

    @Column(nullable = false, updatable = false)
    private String guid = UUIDGenerator.generate();

    @Version
    @Column(name = "version", nullable = false)
    @XmlTransient
    private Long version;

    private Long checkingHashCode;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date enteredDate;
    
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    protected Date modifiedDate;

    /**
     * Записи не удаляются из системы. Они помечаются удаленными. Для этого
     * используется ЭТО поле Если deletedDate == null - значит запись не удалена
     */
    private Date deletedDate;

    @ManyToOne(fetch = FetchType.LAZY)    
    @XmlTransient
    private Oper createdBy;

    protected int status;
    
    @PrePersist
    public void beforeCreate() {
        enteredDate = new Date();
        
        modifiedDate = new Date();

        onBeforeCreate();
    }

    @PreUpdate
    public void beforeUpdate() {
        modifiedDate = new Date();
        onBeforeUpdate();
    }

    protected void onBeforeCreate() {
    }


    protected void onBeforeUpdate() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Long getVersion() {
        return this.version;
    }

    public void setVersion(final Long version) {
        this.version = version;
    }

    public String getGuid() {
        return guid;
    }

    public Long getCheckingHashCode() {
        return checkingHashCode;
    }

    public void setCheckingHashCode(Long checkingHashCode) {
        this.checkingHashCode = checkingHashCode;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date openDate) {
        this.enteredDate = openDate;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

    public Oper getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Oper createdBy) {
        this.createdBy = createdBy;
    }

    public boolean isDeleted() {
        return deletedDate != null;
    }

    public void delete() {
        deletedDate = new Date();
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        if (id != null) {
            result += "id: " + id;
        }
        return result;
    }

    @Override
    public boolean equals(Object o) {
          if (o == null) {
            return false;
        } else if (this == o) {
            return true;
        } else if (!(o instanceof BaseEntity)) {
            return false;
        }

        final BaseEntity that = (BaseEntity) o;

        return Obj.eq(guid, ((BaseEntity) that).getGuid(), false);
    }

    @Override
    public int hashCode() {
        if (guid == null) {
            return super.hashCode();
        } else {
            return guid.hashCode();
        }
    }

    /**
     * Indicates whether the entity has been persisted. This is determined by
     * checking whether the optimistic locking version number is {@code null}.
     * If optimistic locking version number is {@code null}, then the entity has
     * not been persisted; otherwise it has.
     *
     * @return {@code true} if the entity has been persisted; {@code false}
     * otherwise
     */
    public boolean isPersistent() {
        return version != null;
    }
}
