package org.jbalance.core.model.o;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;

@Entity
@NamedQueries({ //	@NamedQuery(name="Juridical.All",query="from Juridical o where o.version is null and o.closeDate is null order by o.name"),
//	@NamedQuery(name="Juridical.AllCount",query="select count(o) from Juridical o where o.version is null and o.closeDate is null"),
//	@NamedQuery(name="Juridical.Search",query="from Juridical o where o.version is null and o.closeDate is null and (o.name like :Pref or o.INN like :Pref or o.barCode like :Pref or o.JAddress like :Pref or o.PAddress like :Pref) order by o.name"),
//	//@NamedQuery(name="Juridical.SearchCount",query="select count(o) from Juridical o where o.version is null and o.closeDate is null and (o.name like :Pref or o.INN like :Pref or o.barCode like :Pref) order by o.name")
//	@NamedQuery(name="Juridical.SearchCount",query="select count(o) from Juridical o where o.version is null and o.closeDate is null and (o.name like :Pref or o.INN like :Pref or o.barCode like :Pref or o.JAddress like :Pref or o.PAddress like :Pref)"),
//	
//	@NamedQuery(name="Juridical.SearchSameName", query="from Juridical o where o.version is null and o.closeDate is null and (lower(o.name) like lower(:Name)) order by o.name"),
//	
//	@NamedQuery(name="Juridical.AllDeleted",query="from Juridical o where o.version is null and o.closeDate is not null order by o.name"),
//	@NamedQuery(name="Juridical.SearchDeleted",query="from Juridical o where o.version is null and o.closeDate is not null and (o.name like :Pref or o.INN like :Pref or o.barCode like :Pref or o.JAddress like :Pref or o.PAddress like :Pref) order by o.name"),
//	
//	@NamedQuery(name="Juridical.AllDeletedCount",query="select count(o) from Juridical o where o.version is null and o.closeDate is not null"),
//	@NamedQuery(name="Juridical.SearchDeletedCount",query="select count(o) from Juridical o where o.version is null and o.closeDate is not null and (o.name like :Pref or o.INN like :Pref or o.barCode like :Pref or o.JAddress like :Pref or o.PAddress like :Pref)")
})
public class Juridical extends Contractor {

    /**
     *
     */
    private static final long serialVersionUID = -1722455974258219396L;

    private String name;

    /**
     * Юридический адрес
     */
    private String jAddress;

    private Long okonh;

    private Long okpo;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getjAddress() {
        return jAddress;
    }

    public void setjAddress(String jAddress) {
        this.jAddress = jAddress;
    }

    public Long getOkonh() {
        return okonh;
    }

    public void setOkonh(Long okonh) {
        this.okonh = okonh;
    }

    public Long getOkpo() {
        return okpo;
    }

    public void setOkpo(Long okpo) {
        this.okpo = okpo;
    }

    @Override
    public String toString() {
        return name;
    }

}
