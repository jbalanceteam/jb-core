package org.jbalance.core.model.o;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import org.jbalance.core.model.BaseDirectoryEntity;

/**
 * @author dima
 */
@Entity
@NamedQueries({ //	@NamedQuery(name="Contractor.All",query="from Contractor o where o.version is null and o.closeDate is null order by o.name"),
//	@NamedQuery(name="Contractor.AllCount",query="select count(o) from Contractor o where o.version is null and o.closeDate is null"),
//	@NamedQuery(name="Contractor.Search",query="from Contractor o where o.version is null and o.closeDate is null and (o.name like :Pref or o.INN like :Pref or o.barCode like :Pref or o.PAddress like :Pref or o.contactInfo like :Pref) order by o.name"),
//	@NamedQuery(name="Contractor.SearchCount",query="select count(o) from Contractor o where o.version is null and o.closeDate is null and (o.name like :Pref or o.INN like :Pref or o.barCode like :Pref or o.PAddress like :Pref or o.contactInfo like :Pref)")
})
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Contractor extends BaseDirectoryEntity {

    /**
     *
     */
    private static final long serialVersionUID = 2625044317286755343L;
    /**
     * ИНН
     */
    private Long inn;
    /**
     * Физический адрес
     */
    private String pAddress;
    private String contactInfo;
    private String phone;
    private String eMail;
    private String description;

    public Long getInn() {
        return inn;
    }

    public void setInn(Long inn) {
        this.inn = inn;
    }

    public String getpAddress() {
        return pAddress;
    }

    public void setpAddress(String pAddress) {
        this.pAddress = pAddress;
    }

    public String getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
