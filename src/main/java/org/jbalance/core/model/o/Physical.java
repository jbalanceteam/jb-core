package org.jbalance.core.model.o;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import org.jbalance.core.model.BaseEntity;

/**
 * Физическое лицо
 *
 * @author dima
 */
@Entity
@NamedQueries({ //	@NamedQuery(name="Physical.All",query="from Physical o where o.deletedDate is null "),
//	@NamedQuery(name="Physical.AllCount",query="select count(o) from Physical o  where o.deletedDate is null "),
//	@NamedQuery(name="Physical.Search",query="from Physical o where o.version is null and o.closeDate is null and (o.name like :Pref or o.surname like :Pref or o.barCode like :Pref) order by o.name"),
//	@NamedQuery(name="Physical.SearchCount",query="select count(o) from Physical o where o.version is null and o.closeDate is null and (o.name like :Pref or o.surname like :Pref or o.barCode like :Pref) order by o.name")
})
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Physical extends BaseEntity implements Serializable {

    private static final long serialVersionUID = -4426847307209004511L;
    /**
     * Имя
     */
    private String realName;
    /**
     * Фамилия
     */
    private String surname;
    /**
     * Отчество
     */
    private String patronymic;
    /**
     * Серия и номер паспорта TODO физ.лицо может иметь несколько паспортов?
     */
    private String passport;

//	private String phone; 
    private String fax;
    /**
     * Дата рождения
     */
    private Date birthday;

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

}
