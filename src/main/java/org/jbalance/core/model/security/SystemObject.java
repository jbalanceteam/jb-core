package org.jbalance.core.model.security;

import javax.persistence.Entity;

import org.jbalance.core.model.BaseEntity;

/**
 * Объект системы
 *
 * @author apv
 */
@Entity
public class SystemObject extends BaseEntity {

    /**
     *
     */
    private static final long serialVersionUID = 4276156980772183791L;

    /**
     * Наименование объекта.
     */
    private String name;
    /**
     * Описание объекта
     */
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
