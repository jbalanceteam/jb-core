package org.jbalance.core.model.security;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.jbalance.core.model.o.Physical;

/**
 * Пользователь системы
 *
 * @author Petr Aleksandrov apv@jbalance.org
 */
@Entity
@NamedQueries({
    //	@NamedQuery(name="Oper.All",query="from Oper o where o.version is null and o.closeDate is null order by o.name"),
    @NamedQuery(name = "Oper.AllCount", query = "select count(o) from Oper o where o.deletedDate is null"),
//	@NamedQuery(name="Oper.Search",query="from Oper o where o.version is null and o.closeDate is null and (o.name like :Pref or o.login like :Pref or o.barCode like :Pref) order by o.name"),
//	@NamedQuery(name="Oper.SearchCount",query="select count(o) from Oper o where o.version is null and o.closeDate is null and (o.name like :Pref or o.login like :Pref or o.barCode like :Pref) order by o.name"),
    @NamedQuery(name = "Oper.ByLogin", query = "from Oper o where o.deletedDate is null and o.login=:Login")
})
@Table(uniqueConstraints = {
    @UniqueConstraint(columnNames = {"login", "deletedDate"})
})
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Oper extends Physical {

    /**
     *
     */
    private static final long serialVersionUID = 4276156980772183791L;
    private static final int COLUMN_LENGTH_LOGIN = 25;

    @Column(length = COLUMN_LENGTH_LOGIN, nullable = false, unique = true)
    @NotNull
    private String login;

    private String passwordHash;

    @OneToMany(mappedBy = "oper", cascade = {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH}, orphanRemoval = true)
    private Set<OperRole> operRoles = new LinkedHashSet<OperRole>();

    @OneToMany(mappedBy = "oper", fetch = FetchType.EAGER, cascade = {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH,CascadeType.DETACH}, orphanRemoval = true)    
    private Set<OperRight> operRights = new LinkedHashSet<OperRight>();

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public Set<OperRole> getOperRoles() {
        return operRoles;
    }

    public void setOperRoles(Set<OperRole> operRoles) {
        this.operRoles = operRoles;
    }

    public Set<OperRight> getOperRights() {
        return operRights;
    }

    public void setOperRights(Set<OperRight> operRights) {
        this.operRights = operRights;
    }

}
