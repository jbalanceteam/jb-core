package org.jbalance.core.model.security;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

import org.jbalance.core.model.BaseEntity;

/**
 *
 * @author Petr Aleksandrov apv@jbalance.org
 */
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public class OperRight extends BaseEntity {

    private static final long serialVersionUID = 4276156980772183791L;

    @ManyToOne(optional = false)
    private Oper oper;

    @ManyToOne(fetch = FetchType.LAZY)
    private SystemAction systemAction;

    public Oper getOper() {
        return oper;
    }

    public void setOper(Oper oper) {
        this.oper = oper;
    }

    public SystemAction getSystemAction() {
        return systemAction;
    }

    public void setSystemAction(SystemAction systemAction) {
        this.systemAction = systemAction;
    }

}
