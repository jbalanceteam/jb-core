package org.jbalance.core.model.security;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.jbalance.core.model.BaseEntity;


/**
 * @author  apv
 */
@Entity
public class RoleRight extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4276156980772183791L;
		
	/**
	 * 
	 */
	private Role role;

	/**
	 * 
	 */
	private SystemAction systemAction;

	@ManyToOne(cascade=CascadeType.ALL,optional=false)
	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	@ManyToOne(cascade=CascadeType.ALL,optional=false)
	public SystemAction getSystemAction() {
		return systemAction;
	}

	public void setSystemAction(SystemAction systemAction) {
		this.systemAction = systemAction;
	}

	
}
