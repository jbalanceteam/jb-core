package org.jbalance.core.model.security;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.jbalance.core.model.BaseEntity;

/**
 * Действие над объектом системы
 *
 * @author apv
 */
@Entity
public class SystemAction extends BaseEntity {

    /**
     *
     */
    private static final long serialVersionUID = 4276156980772183791L;

    /**
     * Наименование действия.
     */
    private String name;
    /**
     * Описание действия
     */
    private String description;
    /**
     * Объект
     */
    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private SystemObject systemObject;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SystemObject getSystemObject() {
        return systemObject;
    }

    public void setSystemObject(SystemObject systemObject) {
        this.systemObject = systemObject;
    }

    @Override
    public String toString() {
        return getSystemObject().getName() + '-' + getName();
    }

}
