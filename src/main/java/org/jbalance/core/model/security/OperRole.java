package org.jbalance.core.model.security;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.jbalance.core.model.BaseEntity;

/**
 * Это связь пользователей с ролями Oper - Role - отношения - many to many Мы не
 * будем использовать встроенную в hibernate/jpa аннотацию ManyToMany Вместо
 * этого будем использовать это entity
 *
 * @author Petr Aleksandrov apv@jbalance.org
 */
@Entity
public class OperRole extends BaseEntity {

    /**
     *
     */
    private static final long serialVersionUID = 4276156980772183791L;

    @ManyToOne(optional = false)
    private Oper oper;

    @ManyToOne(optional = false)
    private Role role;

    public Oper getOper() {
        return oper;
    }

    public void setOper(Oper oper) {
        this.oper = oper;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

}
