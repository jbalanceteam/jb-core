package org.jbalance.core.model.security;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;

import org.jbalance.core.model.BaseEntity;



/**
 * Роль в системе
 * @author  apv
 */
@Entity
@PrimaryKeyJoinColumn(name = "UId")
public class Role extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4276156980772183791L;
		
	/**
	 * Наименование роли.
	 * Например: бухгалтер, администратор...
	 */
	private String name;
	/**
	 * Описание роли
	 */
	private String description;
	/**
	 * Пользователи, принадлежащие к роли
	 */
	@OneToMany(mappedBy="role", cascade= {CascadeType.ALL})
	private Set<OperRole> operRoles = new HashSet<OperRole>();
	
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public Set<OperRole> getOperRoles() {
		return operRoles;
	}

	public void setOperRoles(Set<OperRole> operRoles) {
		this.operRoles = operRoles;
	}
	
	
}
