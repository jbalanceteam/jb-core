package org.jbalance.core.model;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import org.jbalance.core.model.o.Juridical;
import org.jbalance.core.utils.Obj;
import org.jbalance.core.utils.UUIDGenerator;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
@MappedSuperclass
@XmlAccessorType(XmlAccessType.FIELD)
public class BaseDirectoryEntity implements Serializable {

    @Id
    protected Long id;

    private String guid = UUIDGenerator.generate();

    @ManyToMany    
    @XmlTransient
    private Set<Juridical> juridicals = new LinkedHashSet<Juridical>();

    @Column(nullable = false,updatable = false)
    protected Date enteredDate= new Date();

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    protected Date modifiedDate;

    @Column
    @Temporal(TemporalType.TIMESTAMP)    
    @XmlTransient
    protected Date syncDate = new Date();

    /**
     * Записи не удаляются из системы. Они помечаются удаленными. Для этого
     * используется ЭТО поле Если deletedDate == null - значит запись не удалена
     */
    private Date deletedDate;

    public BaseDirectoryEntity() {
    }

    public BaseDirectoryEntity(Long id, Date enteredDate, Date deleted) {
        this.id = id;
        this.enteredDate = enteredDate;
        this.deletedDate = deleted;
    }

    /**
     *
     */
//    @Version
//    @Column(nullable = false)
//    private Long version;
    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Date setDeleted() {
        return deletedDate;
    }

    public void setDeleted(Date deleted) {
        this.deletedDate = deleted;
    }

    public Set<Juridical> getJuridicalOwners() {
        return juridicals;
    }

    public void setJuridicalOwners(Set<Juridical> juridicals) {
        this.juridicals = juridicals;
    }

    public Date getSyncDate() {
        return syncDate;
    }

    public void setSyncDate(Date syncDate) {
        this.syncDate = syncDate;
    }

    /**
     * Returns the optimistic locking version number
     *
     * @return the optimistic locking version number
     */
//    public Long getVersion() {
//        return version;
//    }
    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        } else if (this == o) {
            return true;
        } else if (!(o instanceof BaseDirectoryEntity)) {
            return false;
        }

        final BaseDirectoryEntity that = (BaseDirectoryEntity) o;

//        return Obj.eq(id, that.getId(), false);
        // Use the UUID assigned during instantiation to determine equality.
        // This is more reliable than using the primary key ID, since this
        // value does not change after the entity is persisted.
        return Obj.eq(guid, that.getGuid(), false);
    }

    /**
     * Generates a hash code based on the UUID assigned to the entity instance.
     * It is expected that this behavior will be overridden in subclasses where
     * needed.
     *
     * @return The hash code for the entity based on the primary key value
     */
    @Override
    public int hashCode() {
        // Use the UUID assigned during instantiation to determine hash code.
        // This is more reliable than using the primary key ID, since this
        // value does not change after the entity is persisted.
        //return uuid.hashCode();
        if (guid == null) {
            return super.hashCode();
        } else {
            return guid.hashCode();
        }
    }

    /**
     * Returns a map of name/value pairs to include in the output of the default
     * {@link #toString() toString} method implementation. Subclasses may add,
     * remove, or replace values from the {@code Maps} produced by their
     * superclass implementations. The {@code id}, {@code uuid}, {@code
     * version}, {@code createdDate}, and {@code deletedDate} fields and values
     * are included by default and will be ignored if found in this map. The
     * default {@code Map} implementation is a {@code LinkedHashMap}, so FIFO
     * ordering will be preserved unless overridden.
     *
     * @return a map of name/value pairs to include in the output of the default
     * {@code toString} method.
     */
    protected Map<String, Object> getToStringMap() {
        return new LinkedHashMap<String, Object>();
    }

//    /**
//     * <p>Returns a string representation of the entity suitable for logging
//     * that includes the simple name of the class, primary key, optimistic
//     * locking version number, and soft-deletion date, and any key/value pairs
//     * returned by {@link #getToStringMap()} as applicable in the following
//     * format:</p>
//     *
//     * <p><tt><i>class</i>([id=<i>id</i>, version=<i>version</i>][, deletedDate=<i>deletedDate</i>][, <i>key</i>=<i>value</i>, ...])</tt></p>
//     *
//     * <p>The {@code id} and {@code version} are presented only if the
//     * entity is persistence.  The {@code deletedDate} is presented only if it
//     * is not {@code null}.</p>
//     *
//     * @return a string representation of the entity
//     */
//    @Override
//    public String toString() {
//        SimpleDateFormat shortDateTime = new SimpleDateFormat("yyyyMMddHHmmss.S");
//
//        StringBuilder string = new StringBuilder(getClass().getSimpleName());
//        string.append("(");
//        if(isPersistent()) {
//            string.append("id=").append(id);
//        } else {
//            // Only show if not persistent
//            string.append("uuid=").append(uuid);
//        }
//
//        string.append(", createdDate=").append(shortDateTime.format(createdDate));
//
//        if(deletedDate != null) {
//            string.append(", deletedDate=").append(shortDateTime.format(deletedDate));
//        }
//
//        final Set<String> reservedKeys = new TreeSet<String>();
//        reservedKeys.add("uuid");
//        reservedKeys.add("id");
//        reservedKeys.add("version");
//        reservedKeys.add("createdDate");
//        reservedKeys.add("deletedDate");
//
//        // Get class-specific name/value pairs and append them.  This provides
//        // a consistent, generalized form for entities that have no natural
//        // string representation that is useful for log statements and
//        // debugging.  Subclasses are always free to override the default
//        // toString method for more customized behavior.
//        Map<String, Object> toStringMap = new LinkedHashMap<String, Object>(getToStringMap());
//
//        // Clear out keys that are used by default so that they aren't rendered
//        // twice with possibly conflicting values.
//        toStringMap.keySet().removeAll(reservedKeys);
//
//        if(toStringMap != null && !toStringMap.isEmpty()) {
//            for(Entry<String, Object> entry: getToStringMap().entrySet()) {
//                Object value = entry.getValue();
//                if(value instanceof Date) {
//                    value = shortDateTime.format((Date)value);
//                }
//                string.append(", ").append(entry.getKey()).append('=').append(value);
//            }
//        }
//
//        string.append(")");
//        return string.toString();
//    }
//    
    /**
     * Indicates whether the entity has been persisted. This is determined by
     * checking whether the optimistic locking version number is {@code null}.
     * If optimistic locking version number is {@code null}, then the entity has
     * not been persisted; otherwise it has.
     *
     * @return {@code true} if the entity has been persisted; {@code false}
     * otherwise //
     */
//    public boolean isPersistent() {
//        return version != null;
//    }
}
