package org.jbalance.core.exception;

//import net.cypresscom.cosmos.client.ValidationResponse;
//import net.cypresscom.cosmos.client.ValidationResult;
//import net.cypresscom.cosmos.client.exception.ClientException;
//import net.cypresscom.cosmos.client.exception.ClientExceptionDetail;


public class ValidationException extends JBalanceException {

    private static final long serialVersionUID = 1L;

//    private ValidationResponse validationResponse;

    public ValidationException() {
    }

    public ValidationException(String message) {
        super(message);
    }

    public ValidationException(Throwable t) {
        super(t);
    }

//    public ValidationException(ValidationResponse validationResponse) {
//        super(validationResponse.toString());
//        this.setValidationResponse(validationResponse);
//    }
//
//    public void setValidationResponse(ValidationResponse validationResponse) {
//        this.validationResponse = validationResponse;
//    }
//
//    public ValidationResponse getValidationResponse() {
//        return validationResponse;
//    }
//
//    @Override
//    public String getMessage() {
//        if (validationResponse == null) {
//            return super.getMessage();
//        }
//        return validationResponse.toString();
//    }
//
//    public ClientException asClientException() {
//        ClientException clientException = new ClientException();
//        if(validationResponse != null) {
//            for(ValidationResult validationResult: validationResponse.getResults()) {
//                ClientExceptionDetail detail = new ClientExceptionDetail(validationResult.getMessageKey(), validationResult.getPropertyPath(), validationResult.getInvalidValue(), validationResult.getMessage());
//                clientException.addDetail(detail);
//            }
//        }
//        return clientException;
//    }
}
