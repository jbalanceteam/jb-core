package org.jbalance.core.exception;

import org.jbalance.core.utils.Str;


/**
 * Base class for all checked JBalance application exceptions
 * 
 */
public class JBalanceException extends Exception {
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new, blank COSMOS exception
     */
    public JBalanceException() {
    }

    /**
     * Creates a new COSMOS exception with the specified detail message
     *
     * @param message A message providing details about the exception
     */
    public JBalanceException(String message) {
        super(message);
    }

    /**
     * Creates a new COSMOS exception with the specified detail message
     *
     * @param messagePattern A message providing details about the exception
     * @param arguments Arguments to replace in the message pattern
     *
     * @see Str#format(java.lang.String, java.lang.Object[]) 
     */
    public JBalanceException(String messagePattern, Object... arguments) {
        super(Str.format(messagePattern, arguments));
    }

    /**
     * Creates a new COSMOS exception with the specified detail message and
     * nested throwable
     *
     * @param message A message providing details about the exception
     * @param cause The throwable that caused the COSMOS exception
     */
    public JBalanceException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Creates a new COSMOS exception with the specified detail message and
     * nested throwable
     *
     * @param messagePattern A message providing details about the exception
     * @param cause The throwable that caused the COSMOS exception
     * @param arguments Arguments to replace in the message pattern
     *
     * @see Str#format(java.lang.String, java.lang.Object[]) 
     */
    public JBalanceException(String messagePattern, Throwable cause, Object... arguments) {
        super(Str.format(messagePattern, arguments), cause);
    }

    /**
     * Creates a new COSMOS exception with the specified nested throwable
     *
     * @param cause The throwable that caused the COSMOS exception
     */
    public JBalanceException(Throwable cause) {
        super(cause);
    }
}
