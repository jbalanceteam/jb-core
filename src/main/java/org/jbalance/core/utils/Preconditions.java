package org.jbalance.core.utils;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.jbalance.core.exception.JBalanceException;
import org.jbalance.core.exception.ValidationException;
import org.jbalance.core.model.BaseEntity;



import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;


/**
 * Simple static methods to be called at the start of your own methods to verify
 * correct arguments and state. This allows constructs such as
 *
 * <pre>
 * if (enterprise == null) {
 *     throw new IllegalArgumentException(&quot;Enterprise is required&quot;);
 * }
 * </pre>
 *
 * to be replaced with the more compact
 *
 * <pre>
 * checkNotNull(enterprise, &quot;Enterprise is required&quot;);
 * </pre>
 *
 * This class provides Cypress-specific functionality not included in Google's
 * {@link com.google.common.base.Preconditions Preconditions class}, and wraps
 * methods that already exist in that library to provide argument checking
 * consistent with Cypress's policy of throwing {@link IllegalArgumentException}
 * when an argument is {@code null}. The methods are wrapped for consistency and
 * to avoid reimplementing existing functionality that is already covered by
 * unit tests
 *
 * @author dzeigler
 *
 */
public final class Preconditions {

    private static Log log = new Log(Preconditions.class);

    /**
     * Ensures that an object reference passed as a parameter to the calling
     * method is not null.
     *
     * Wraps {@link com.google.common.base.Preconditions#checkArgument(boolean)}
     * for consistency.
     *
     * @param <T> 
     * @param reference
     *            an object reference
     * @throws IllegalArgumentException
     *             if the {@code reference} is null
     * @return the non-null reference that was validated
     */
    public static <T> T checkNotNull(T reference) {
        com.google.common.base.Preconditions.checkArgument(reference != null);
        return reference;
    }

    /**
     * Ensures that an object reference passed as a parameter to the calling
     * method is not null.
     *
     * Wraps
     * {@link com.google.common.base.Preconditions#checkArgument(boolean, Object)}
     * for consistency.
     *
     * @param <T>
     * @param reference
     *            an object reference
     * @param errorMessage
     *            the exception message to use if the check fails; will be
     *            converted to a string using {@link String#valueOf(Object)}
     * @throws IllegalArgumentException
     *             if the {@code reference} is null
     * @return the non-null reference that was validated
     */
    public static <T> T checkNotNull(T reference, Object errorMessage) {
        com.google.common.base.Preconditions.checkArgument(reference != null, errorMessage);
        return reference;
    }

    /**
     * Ensures that an object reference passed as a parameter to the calling
     * method is not null.
     *
     * Wraps
     * {@link com.google.common.base.Preconditions#checkArgument(boolean, Object, Object...)}
     * for consistency.
     *
     * @param <T>
     * @param reference
     *            an object reference
     * @param errorMessageTemplate
     *            a template for the exception message should the check fail.
     *            The message is formed by replacing each {@code %s} placeholder
     *            in the template with an argument. These are matched by
     *            position - the first {@code %s} gets
     *            {@code errorMessageArgs[0]}, etc. Unmatched arguments will be
     *            appended to the formatted message in square braces. Unmatched
     *            placeholders will be left as-is.
     * @param errorMessageArgs
     *            the arguments to be substituted into the message template.
     *            Arguments are converted to strings using {@link
     *            String.valueOf(Object)}.
     * @throws IllegalArgumentException
     *             if the {@code reference} is null
     * @return the non-null reference that was validated
     */
    public static <T> T checkNotNull(T reference, String errorMessageTemplate, Object... errorMessageArgs) {
        com.google.common.base.Preconditions.checkArgument(reference != null, errorMessageTemplate, errorMessageArgs);
        return reference;
    }

    public static <T extends Collection<?>> T checkNotEmpty(T reference, String errorMessageTemplate, Object... errorMessageArgs) {
        com.google.common.base.Preconditions.checkArgument(reference != null && !reference.isEmpty(), errorMessageTemplate, errorMessageArgs);
        return reference;
    }

//    public static void checkArgument(boolean expression, String errorMessageTemplate, Object... errorMessageArgs) throws JBalanceException {
//        com.google.common.base.Preconditions.checkArgument(expression, errorMessageTemplate, errorMessageArgs);
//    }

//    /**
//     * Checks is {@code effectiveDate} is {@code null} and if it is, returns a
//     * {@code new Date()}. If it is not, it returns {@code effectiveDate}
//     *
//     * @param effectiveDate
//     * @return if effectiveDate is null, returns new Date(); else returns the
//     *         passed in effectiveDate
//     */
//    public static Date checkEffectiveDate(Date effectiveDate) {
//        if (effectiveDate == null) {
//            return new Date();
//        }
//        return effectiveDate;
//    }
//
//    public static <T extends BaseEntity> T checkId(EntityManager em, Class<T> entityClass, Object primaryKey, String propertyName) throws ValidationException {
//        return checkId(em, entityClass, primaryKey, propertyName, false);
//    }
//
//    public static <T extends BaseEntity> T checkId(EntityManager em, Class<T> entityClass, Object primaryKey, String propertyName, boolean allowDeleted) throws ValidationException {
//        boolean error = false;
//        T entity = null;
//        try {
//            entity = em.find(entityClass, primaryKey);
//            if (entity == null || (!allowDeleted && entity.isDeleted())) {
//                error = true;
//            }
//        } catch (Exception e) {
//            log.info("{0}: Exception while checking id {1} for class {2}", e, Log.Replacement.METHOD_NAME, primaryKey, entityClass);
//            error = true;
//        }
//        if (entity == null || error) {
//            ValidationResponse validationResponse = new ValidationResponse();
//            validationResponse.addResourceResult(ValidationSeverity.ERROR, CosmosClientConstants.VALIDATOR_MESSAGE_CHECK_ID_NOT_FOUND, primaryKey, propertyName);
//            throw new ValidationException(validationResponse);
//        }
//        return entity;
//    }
//
//    @SuppressWarnings("unchecked")
//    public static <T extends BaseEntity> Collection<T> checkIds(EntityManager em, Class<T> entityClass, Set<? extends Object> primaryKey, String propertyName) throws ValidationException {
//        boolean error = false;
//        List<T> resultList = null;
//        Map<Object, Boolean> entities = null;
//        SetView<? extends Object> keysNotFound = null;
//        try {
//            StringBuilder ejbql = new StringBuilder("select e from ").append(entityClass.getName()).append(" e where e.id in (:primaryKey)");
//            Query query = em.createQuery(ejbql.toString());
//            resultList = query.getResultList();
//            entities = Maps.newHashMap();
//            for (T entity : resultList) {
//                if (entity.exists()) {
//                    error = true;
//                }
//                entities.put(entity.getId(), error);
//            }
//            keysNotFound = Sets.difference(primaryKey, entities.keySet());
//            if (keysNotFound != null && !keysNotFound.isEmpty()) {
//                error = true;
//            }
//        } catch (Exception e) {
//            log.info("{0}: Exception while checking id {1} for class {2}", e, Log.Replacement.METHOD_NAME, primaryKey, entityClass);
//            error = true;
//        }
//        if (error || entities == null || entities.isEmpty()) {
//            ValidationResponse validationResponse = new ValidationResponse();
//            for (Object key : keysNotFound) {
//                validationResponse.error(CosmosClientConstants.VALIDATOR_MESSAGE_CHECK_ID_NOT_FOUND, key, propertyName);
//            }
//            if (entities != null) {
//                for (Entry<Object, Boolean> entry : entities.entrySet()) {
//                    if (entry.getValue()) {
//                        validationResponse.error(CosmosClientConstants.VALIDATOR_MESSAGE_CHECK_ID_EXISTENCE, entry.getKey(), propertyName);
//                    }
//                }
//            }
//            throw new ValidationException(validationResponse);
//        }
//        return resultList;
//    }
//
    public static <T extends BaseEntity> T checkId2(EntityManager em, Class<T> entityClass, Object primaryKey, String propertyName) throws ValidationException {
        return checkId2(em, entityClass, primaryKey, propertyName, "ENTITY_NOT_FOUND", false);
    }

    public static <T extends BaseEntity> T checkId2(EntityManager em, Class<T> entityClass, Object primaryKey, String propertyName, String messageKey) throws ValidationException {
        return checkId2(em, entityClass, primaryKey, propertyName, "ENTITY_NOT_FOUND", false);
    }

    public static <T extends BaseEntity> T checkId2(EntityManager em, Class<T> entityClass, Object primaryKey, String propertyName, boolean allowDeleted) throws ValidationException {
        return checkId2(em, entityClass, primaryKey, propertyName, "ENTITY_NOT_FOUND", allowDeleted);
    }
//
    public static <T extends BaseEntity> T checkId2(EntityManager em, Class<T> entityClass, Object primaryKey, String propertyName, String messageKey, boolean allowDeleted) throws ValidationException {
        if(Str.empty(messageKey)) {
            messageKey = "ENTITY_NOT_FOUND";
        }

        boolean error = false;
        T entity = null;
        try {
            entity = em.find(entityClass, primaryKey);
            if (entity == null || (!allowDeleted && entity.isDeleted())) {
                error = true;
            }
        } catch (IllegalArgumentException e) {
            log.debug("{0}: no {1} record found for {2}", e, Log.Replacement.METHOD_NAME, entityClass.getSimpleName(), primaryKey);
            error = true;
        } catch (Exception e) { 
            log.debug("{0}: the entity manager has been closed or an unknown error occurred; entityClass={1}; primaryKey={2}", e, Log.Replacement.METHOD_NAME, entityClass.getSimpleName(), primaryKey);
            throw new ValidationException("ERROR_LOADING_ENTITY");
        }
        if (error) {
            throw new ValidationException("Could not retrieve entity by ID.  See logs near this timestamp: " + new Date());
        }
        return entity;
    }

    public static <T extends BaseEntity> T checkOptionalId2(EntityManager em, Class<T> entityClass, Object primaryKey, String propertyName) throws ValidationException {
        if (Str.empty(primaryKey)) {
            return null;
        }
        return checkId2(em, entityClass, primaryKey, propertyName, "ENTITY_NOT_FOUND", false);
    }
    /**
     *
     * @param em
     *            (required)
     * @param entityClass
     *            (required)
     * @param primaryKey
     *            (required)
     * @param propertyName
     * @return
     * @throws JBalanceException
     * @throws {@link IllegalArgumentException} when {@code em},
     *         {@code entityClass}, or {@code primaryKey} are <code>null</code>.
     */
    public static <T extends BaseEntity> Collection<T> checkIds2(EntityManager em, Class<T> entityClass, Set<? extends Object> primaryKey, String propertyName) throws JBalanceException {
        return checkIds2(em, entityClass, primaryKey, propertyName, "ENTITY_NOT_FOUND");
    }
//
//    /**
//     *
//     * @param em
//     *            (required)
//     * @param entityClass
//     *            (required)
//     * @param primaryKey
//     *            (required)
//     * @param propertyName
//     * @param messageKey
//     * @return
//     * @throws JBalanceException
//     * @throws {@link IllegalArgumentException} when {@code em},
//     *         {@code entityClass}, or {@code primaryKey} are <code>null</code>.
//     */
    public static <T extends BaseEntity> Collection<T> checkIds2(EntityManager em, Class<T> entityClass, Set<? extends Object> primaryKey, String propertyName, String messageKey) throws JBalanceException {
        checkNotNull(em, "An EntityManager is required");
        checkNotNull(entityClass, "The entity Class is required");
        com.google.common.base.Preconditions.checkArgument(primaryKey != null && !primaryKey.isEmpty(), "Primary key required; Unable to check the id for class [%s] based on property [%s].", entityClass.getSimpleName(), propertyName);
        if(Str.empty(messageKey)) {
            messageKey = "ENTITY_NOT_FOUND";
        }

        boolean error = false;
        List<T> resultList = null;
        Map<Object, Boolean> entities = null;
        SetView<? extends Object> keysNotFound = null;
        try {
            StringBuilder ejbql = new StringBuilder("select e from ").append(entityClass.getName()).append(" e where e.id in (:primaryKey)");
            Query query = em.createQuery(ejbql.toString());
            query.setParameter("primaryKey", primaryKey);
            resultList = query.getResultList();
            entities = Maps.newHashMap();
            for (T entity : resultList) {
                boolean entityError = false;
                if (entity.isDeleted()) {
                    error = true;
                    entityError = true;
                }
                entities.put(entity.getId(), entityError);
            }
            keysNotFound = Sets.difference(primaryKey, entities.keySet());
            if (keysNotFound != null && !keysNotFound.isEmpty()) {
                error = true;
            }
        } catch (IllegalArgumentException e) {
            log.debug("{0}: no {1} record found for {2}", e, Log.Replacement.METHOD_NAME, entityClass.getSimpleName(), primaryKey);
            error = true;
        } catch (Exception e) {
            log.debug("{0}: the entity manager has been closed or an unknown error occurred; entityClass={1}; primaryKey={2}", e, Log.Replacement.METHOD_NAME, entityClass.getSimpleName(), primaryKey);
            throw new JBalanceException();
        }
        if (error || entities == null || entities.isEmpty()) {
        	JBalanceException JBalanceException = new JBalanceException();
            for (Object key : keysNotFound) {
//                JBalanceException.addDetail(new JBalanceExceptionDetail(ClientMessageKeys.ENTITY_NOT_FOUND, propertyName, key));
            }
            if (entities != null) {
                for (Entry<Object, Boolean> entry : entities.entrySet()) {
                    if (entry.getValue()) {
//                        JBalanceException.addDetail(new JBalanceExceptionDetail(ClientMessageKeys.ENTITY_NOT_EXISTS, propertyName, entry.getKey()));
                    }
                }
            }
            throw JBalanceException;
        }
        return resultList;
    }
}
