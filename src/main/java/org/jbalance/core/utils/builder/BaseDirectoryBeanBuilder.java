package org.jbalance.core.utils.builder;

/**
 *
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public abstract class BaseDirectoryBeanBuilder<E, P>
{

    protected P proxy;
    protected E entity;

    public BaseDirectoryBeanBuilder()
    {
    }

    public BaseDirectoryBeanBuilder(P p)
    {
        proxy = p;
    }

    public void setProxy(P p)
    {
        proxy = p;
    }

    public void setEntity(E e)
    {
        entity = e;
    }

    public abstract E buildEntity();

    public abstract P buildProxy();
    
}
