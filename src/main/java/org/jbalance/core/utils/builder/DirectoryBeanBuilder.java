package org.jbalance.core.utils.builder;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public interface DirectoryBeanBuilder<E, P>
{

    public void setProxy(P p);

    public E buildEntity();
}
