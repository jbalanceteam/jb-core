package org.jbalance.core.client;

import java.io.Serializable;
import java.util.Properties;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * <p>Базовый класс EJB-клиента JBalance Core.</p>
 * <p>Предоставляет интерфейс для доступа к контексту</p>
 *
 * @author Alexandr Chubenko
 */
public class JBCoreBaseClient implements Serializable {

    private static final long serialVersionUID = -1L;
    
    /**
     * <p>Создает {@link InitialContext} по заданным параметров.</p>
     * <p>Добавляет в передаваемый набор свойств, свойства полученные из {@link JBCoreBaseClient#getDefaultContextProperties()}</p>
     * @param environment Параметры создаваемого контекста.
     * @return {@link InitialContext}
     * @throws NamingException 
     */
    protected Context getContext(Properties environment) throws NamingException {
        if(environment!=null){
            environment.putAll(getDefaultContextProperties());
        }else{
            environment = getDefaultContextProperties();
        }
        
        return new InitialContext(environment);
    }

    /**
     * <p>Возвращает набор свойств по умолчанию.</p>
     * <p>Включает в себя свойство префиксов пакетов, что позволяет разрешить проблему с получением объектов из JNDI</p>
     * @return {@link Properties}
     */
    protected Properties getDefaultContextProperties() {
        Properties properties = new Properties();
        properties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
        return properties;
    }

    /**
     * Создает {@link InitialContext} с параметрами по умолчанию
     * @return Созданный контекст с параметрами по умолчанию.
     * @throws NamingException 
     */
    protected Context getContext() throws NamingException {
        return getContext(null);
    }
    
    /**
     * Позволяет получить ссылку на объект JNDI с заданным именем.
     * @param name Имя объекта JNDI
     * @return Ссылку на объект из JNDI типа {@link Object}
     * @throws NamingException 
     */
    protected Object lookup(String name) throws NamingException{
        return getContext().lookup(name);
    }
}
