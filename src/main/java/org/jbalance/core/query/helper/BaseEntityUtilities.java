package org.jbalance.core.query.helper;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.List;
import java.util.Map;
import org.jbalance.core.model.BaseEntity;


public class BaseEntityUtilities<E extends BaseEntity> {

    /**
     * Utility method that returns a List containing the id of each entity
     * passed in the entities collection.
     *
     * @param entities
     * @return
     */
    public static List<Long> getIds(Iterable<? extends BaseEntity> entities) {
        return Lists.newArrayList(Iterables.transform(entities, Functions.GET_ID));
    }

    /**
     * Utility method that returns a Map keyed on the id of each entity
     * passed in with the entity as the value.
     *
     * @param entities
     * @return
     */
    public static <E extends BaseEntity> Map<Long, E> newIdMap(Iterable<E> entities) {
        Map<Long, E> entityMap = Maps.newHashMap();
        for (E entity : entities) {
            entityMap.put(entity.getId(), entity);
        }
        return entityMap;
    }

    public enum Functions implements Function<BaseEntity, Long> {
        GET_ID {
            @Override
            public Long apply(final BaseEntity entityBase) {
                return entityBase.getId();
            }
        }
    }
}
