package org.jbalance.core.query.juridical;

import javax.xml.bind.annotation.XmlType;
import org.jbalance.core.filter.LongFilter;
import org.jbalance.core.filter.StringFilter;
import org.jbalance.core.query.GetRequest;

/**
 * @author Alexandr Chubenko
 */
@XmlType(propOrder
        = {
            "idFilter",
            "juridicalNameFilter",
            "juridicalInnFilter"
        })
public class GetJuridicalsRequest extends GetRequest<GetJuridicalsResult> {

    private static final long serialVersionUID = 1L;

    private LongFilter idFilter;
    private StringFilter juridicalNameFilter;
    private LongFilter juridicalInnFilter;
    private LongFilter juridicalFilter;

    @Override
    public GetJuridicalsResult createResult() {
        return new GetJuridicalsResult(this);
    }

    public StringFilter getJuridicalNameFilter() {
        return juridicalNameFilter;
    }

    public void setJuridicalNameFilter(StringFilter juridicalNameFilter) {
        this.juridicalNameFilter = juridicalNameFilter;
    }

    public LongFilter getJuridicalInnFilter() {
        return juridicalInnFilter;
    }

    public void setJuridicalInnFilter(LongFilter juridicalInnFilter) {
        this.juridicalInnFilter = juridicalInnFilter;
    }

    public LongFilter getIdFilter() {
        return idFilter;
    }

    public void setIdFilter(LongFilter idFilter) {
        this.idFilter = idFilter;
    }

    public LongFilter getJuridicalFilter() {
        return juridicalFilter;
    }

    public void setJuridicalFilter(LongFilter juridicalFilter) {
        this.juridicalFilter = juridicalFilter;
    }

}
