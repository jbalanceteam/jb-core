
package org.jbalance.core.query.juridical;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.jbalance.core.model.o.Juridical;
import org.jbalance.core.query.GetResult;

/**
 * @author Alexandr Chubenko
 */
@XmlRootElement
public class GetJuridicalsResult extends GetResult {
    private static final long serialVersionUID = 1L;

    private List<Juridical> juridicals;

    public GetJuridicalsResult() {
    }

    @XmlElement(name="juridicals")
    public List<Juridical> getJuridicals() {
        if (juridicals == null) {
        	juridicals = new ArrayList<Juridical>();
        }
        return juridicals;
    }

    public void setJuridicals(List<Juridical> juridicals) {
        this.juridicals = juridicals;
    }

    public GetJuridicalsResult(GetJuridicalsRequest request) {
        super(request);
    }
}
