package org.jbalance.core.query.juridical;

import java.util.List;
import javax.persistence.EntityManager;
import org.jbalance.core.model.o.Juridical;
import org.jbalance.core.query.helper.AbstractQueryBuilder;
import org.jbalance.core.query.helper.GetRequestHelper;
import org.jbalance.core.utils.CollectionUtil;

/**
 *
 */
public class GetJuridicalsRequestHelper extends GetRequestHelper<GetJuridicalsRequest, GetJuridicalsResult> {

    public GetJuridicalsRequestHelper(GetJuridicalsRequest request) {
        super(request);
    }

    @Override
    protected AbstractQueryBuilder<?> initQuery() {
        JuridicalQueryBuilder juridicalQueryBuilder = new JuridicalQueryBuilder();
        juridicalQueryBuilder.appendFilter(request.getIdFilter(), "juridical.id");
        juridicalQueryBuilder.appendFilter(request.getJuridicalNameFilter(), "juridical.name");
        juridicalQueryBuilder.appendFilter(request.getJuridicalInnFilter(), "juridical.inn");
        juridicalQueryBuilder.appendFilter(request.getJuridicalFilter(), "juridicals.id");
        
        juridicalQueryBuilder.orderBy().append("order by juridical.enteredDate desc");
        return juridicalQueryBuilder;
    }


    @Override
    protected GetJuridicalsResult createResult(GetJuridicalsResult result, List<?> records, EntityManager em) {
        List<Juridical> juridicals = (List<Juridical>) records;
        juridicals = CollectionUtil.filterDuplicates(juridicals);
        result.setJuridicals(juridicals);
        return result;
    }
}
