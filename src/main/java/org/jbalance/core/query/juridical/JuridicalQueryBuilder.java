package org.jbalance.core.query.juridical;

import org.jbalance.core.model.o.Juridical;
import org.jbalance.core.query.helper.AbstractQueryBuilder;


/**
 * Построитель запросов для {@link Juridical}
 *
 * @author Alexandr Chubenko
 */
public class JuridicalQueryBuilder extends AbstractQueryBuilder<JuridicalQueryBuilder> {

    public JuridicalQueryBuilder() {
        super(null, Juridical.class, "juridical");
    }

    @Override
    public StringBuilder buildJoins() {
        StringBuilder joins = new StringBuilder()        
                .append("LEFT JOIN FETCH juridical.juridicals juridicals ");//shit
        return joins;
    }

    @Override
    protected JuridicalQueryBuilder get() {
        return this;
    }

}
