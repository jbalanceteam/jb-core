package org.jbalance.core.query;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Indicates a sort that should be performed on {@link Result} results before they
 * are returned from the Service method.  A sort consists of a sortable field and
 * a sort direction, and multiple sorts can be applied to each search.
 *
 */
public abstract class Sort implements Serializable {

    private static final long serialVersionUID = 1L;

    private SortDirection sortDirection;

    public Sort() {

    }

    public Sort(SortDirection sortDirection) {
        this.sortDirection = sortDirection;
    }

    @XmlAttribute
    public SortDirection getSortDirection() {
        return sortDirection;
    }

    public void setSortDirection(SortDirection sortDirection) {
        this.sortDirection = sortDirection;
    }

    public abstract Enum<?> getSortableField();

}
