package org.jbalance.core.sessions;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Предок всех сессионных бинов.
 * Содержит базовые методы, {@link EntityManager}, и т.д.
 * @author Alexandr Chubenko
 */
public class BaseSessionBean  {
    @PersistenceContext
    public EntityManager manager;
}
