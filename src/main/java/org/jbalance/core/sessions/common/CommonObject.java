package org.jbalance.core.sessions.common;

import java.util.List;

import org.jbalance.core.model.BaseEntity;


public interface  CommonObject <D extends BaseEntity> extends Common<D> {
	
	public D create(D object);

	public D delete(D object);

	public D update(D object) ;
	
	public List<? extends D> getAll(Class<D> clazz);
	
	public D getById(Long id);
	
//	
//	public void search();

	public Long getCount(Class<D> clazz);
	
//	public List<? extends BaseEntity> getRange(Long firstRow,Long maxRow);
}
