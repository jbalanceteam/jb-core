package org.jbalance.core.sessions.common;

import java.util.List;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.jbalance.core.model.BaseEntity;

public abstract class CommonObjectBean<D extends BaseEntity> extends CommonBean<D> implements CommonObject<D> {
	protected final static int MAX_SEARCH_STRING_SIZE = 10;

	public static final long serialVersionUID = -3166304145281052141L;

	public abstract Class<D> getClazz();

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public D create(D object) {
		return (D) create_(object);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public  D delete(D object) {
		return delete_(object);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public D update(D object) {
		return update_(object);
	}

	@Override
	public List<? extends D> getAll(Class clazz) {
		return manager.createNamedQuery(clazz.getSimpleName()+ ".All").getResultList();
	}
//
//	public List<? extends BaseEntity> getRange(Long firstRow, Long maxRow) {
//		getLog().debug("getRange("+firstRow+","+maxRow+")");
//		List tmp = constructQuery().setFirstResult(firstRow.intValue())
//				.setMaxResults(maxRow.intValue()).getResultList();
//		return tmp;
//	}

	@Override
	public Long getCount(Class<D> clazz) {
		if(clazz==null)
			throw new RuntimeException();
			return ((Number) manager.createNamedQuery(
					clazz.getSimpleName() + ".AllCount")
					.getSingleResult()).longValue();
	}

	@Override
	public D getById(Long UId) {
		if(UId==null){
			throw new RuntimeException("UId mustn't be null");
		}
		
		if(getClazz()==null){
			throw new RuntimeException("clazz mustn't be null");
		}
		return manager.find(getClazz(), UId);
	}
}
