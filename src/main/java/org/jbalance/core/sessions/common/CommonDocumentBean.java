//package org.jbalance.core.sessions.common;
//
//import java.util.List;
//
//import javax.ejb.TransactionAttribute;
//import javax.ejb.TransactionAttributeType;
//
//import org.itx.jbalance.l0.h.Hdocument;
//import org.itx.jbalance.l0.s.Sdocument;
//
//public class CommonDocumentBean <D extends Hdocument<?>,S extends Sdocument>  extends CommonObjectBean<D> implements
//		CommonDocument<D,S> {
//
//	private static final long serialVersionUID = 6705628333560203373L;
//
//
//	@Override
//	@TransactionAttribute(TransactionAttributeType.REQUIRED)
//	public D create(D object){
//		setupDNumber(object);
//		return super.create(object);
//	}
//
//	
//	protected void setupDNumber(D object) {
//		Long i = getMaxNumber(object);
//		i = i == null ? i = 1l : i+1;
//		object.setDnumber(i);
//	}
//
//	@Override
//	@TransactionAttribute(TransactionAttributeType.REQUIRED)
//	public S createSpecificationLine(S spec, D doc){
//		// Calculate and set new sequental number
//		Integer i = getMaxSeqNumber(spec, doc);
//		i = i == null ? i = new Integer(1) : ++i;
//		spec.setSeqNumber(i);
//		spec.setHUId(doc);
//		return (S) create_(spec);
//	}
//
//	@Override
//	@TransactionAttribute(TransactionAttributeType.REQUIRED)
//	public D delete(D object) {
//		for (S s : getSpecificationLines(object)) {
//			deleteSpecificationLine(s);
//		}
//		return super.delete(object);
//	}
//
//	@TransactionAttribute(TransactionAttributeType.REQUIRED)
//	public S deleteSpecificationLine(S specificationLine) {
//		return delete_(specificationLine);
//	}
//
//
//	@Override
//	@TransactionAttribute(TransactionAttributeType.REQUIRED)
//	public S updateSpecificationLine(S specificationLine) {
//		return update_(specificationLine); //
//	}
//
//	@Override
//	@SuppressWarnings("unchecked")
//	@TransactionAttribute(TransactionAttributeType.REQUIRED)
//	public List<S> getSpecificationLines(D object) {
//		getLog().debug("getSpecificationLines()  for "+object + "   UID: "+ (object==null? null: object.getUId()));
//		if (object==null || object.getUId() == null) {
//			return null;
//		}
//		
//		try{
////			return manager.createNamedQuery(getObject().getClass().getSimpleName()+".Specification")
////			.setParameter("Header", getObject()).getResultList();
//			
//			return manager.createQuery(
//			"from S"+object.getClass().getSimpleName().substring(1)+" o where o.version is null and o.closeDate is null and o.HUId = :Header order by o.seqNumber"
//			).setParameter("Header", object).getResultList();
//		
//		}catch(Exception e){
//			getLog().error("", e);
//			throw new RuntimeException(e);
//		}
//	}
//	
//	
////	/**
////	 * This method is universal,
////	 * but too slow
////	 * @return
////	 */
////	@SuppressWarnings("unchecked")
////	@TransactionAttribute(TransactionAttributeType.REQUIRED)
////	protected List<S> getSpecificationLinesCommon() {
//////		Пиздец шо за запрос сейчас сгенерится :(((
////		try{
////		return manager.createNamedQuery("Document.Specification")
////				.setParameter("Header", getObject()).getResultList();
////		} catch (IllegalStateException Ex) {
////			getLog().error("", Ex);
////			return null;
////		}
////	}
////	
//
//
//	protected Long getMaxNumber(D object) {
//		if(object.getContractorOwner() != null)
//			return (Long) manager.createNamedQuery(
//					object.getClass().getSimpleName() + ".MaxNumber")
//				.setParameter("ContractorOwner",
//						object.getContractorOwner()).getResultList()
//				.get(0);
//		else {
//			//!!!!!!!!!!!!!!!!!!!
////			if(sessionprofile.getConfig().getDivisionjuridical()==null)return 0l;
////			HdivisionJuridical h =  (HdivisionJuridical)sessionprofile.getConfig().getDivisionjuridical().getHUId();
////			return (Long) manager.createNamedQuery(
////					getObject().getClass().getSimpleName() + ".MaxNumber")
////					.setParameter("ContractorOwner", (Juridical)h.getJuridical()
////							).getResultList()
////					.get(0);
//			return 0l;
//		}	
//	}
//
//	private Integer getMaxSeqNumber(S specificationLine, D doc) {
//		return (Integer) manager.createNamedQuery(specificationLine.getClass().getSimpleName()+ ".MaxSeqNumber")
//				.setParameter("Header", doc).getResultList().get(0);
//	}
//
//	
//
////	@Override
////	public void search() {
////		if (getSpecificationLine().getSearchString() != null)
////			setSpecificationDirty(true);
////		if (getObject().getSearchString() != null)
////			super.search();
////	}
//}
