package org.jbalance.core.sessions;

import java.util.Date;
import java.util.List;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.jbalance.core.exception.JBalanceException;
import org.jbalance.core.model.BaseEntity;
import org.jbalance.core.query.GetRequest;
import org.jbalance.core.utils.Log;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 * @param <T> - Реализация класса справочника, наследующая от {@link BaseEntity}
 * @param <S> - Реализация {@link GetRequest}, для конкретного класса
 * справочника <tt>T</tt>
 */
public abstract class GenericSessionBean<T extends BaseEntity> extends BaseSessionBean implements GenericSession<T> {

    /**
     * Логгер.
     */
    protected Log log = new Log(getClass());

    @Override
    public T add(T bean) throws JBalanceException {
        bean.setEnteredDate(new Date());
        manager.persist(bean);
        return bean;
    }

    @Override
    public T delete(long id) throws JBalanceException {
        T bean = getById(id);
        bean.delete();
        return manager.merge(bean);
    }

    @Override
    public T getById(long id) {
        return (T) manager.find(getClazz(), id);
    }

    @Override
    public T edit(T o) throws JBalanceException {
        return manager.merge(o);
    }

    @Override
    public List<T> list() {

        CriteriaBuilder cb = manager.getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(getClazz());
        Root<T> rootEntry = cq.from(getClazz());
        CriteriaQuery<T> all = cq.select(rootEntry);

        all.distinct(true).where(cb.isNull(rootEntry.get("deletedDate"))).orderBy(cb.desc(rootEntry.get("enteredDate")));

        TypedQuery<T> allQuery = manager.createQuery(all);

        return allQuery.getResultList();
    }

    public void validate(T o) throws JBalanceException {
    }
}
