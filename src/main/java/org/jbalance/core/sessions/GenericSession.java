package org.jbalance.core.sessions;

import java.util.List;
import org.jbalance.core.exception.JBalanceException;
import org.jbalance.core.model.BaseEntity;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 * @param <T> - Реализация класса справочника, наследующая от {@link BaseEntity}
 *  <tt>T</tt>
 */
public interface GenericSession<T extends BaseEntity> {

    public Class<T> getClazz();

    public T add(T bean) throws JBalanceException;

    public T delete(long id) throws JBalanceException;

    public T getById(long id);

    public T edit(T bean) throws JBalanceException;

    public List<T> list();
}
