package org.jbalance.core.sessions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.xml.datatype.DatatypeConfigurationException;
import org.jbalance.core.model.BaseDirectoryEntity;
import org.jbalance.core.utils.builder.DirectoryBeanBuilder;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 * @param <E> Warehouse model entity
 * @param <P> Directory model entity
 * @param <R> Get request class for entity
 */
public abstract class BaseDirectoryEntitySessionBean<E extends BaseDirectoryEntity, P, R>
        extends BaseSessionBean
{

    protected Class beanClass;
    protected Class proxyClass;
    protected Class requestClass;
    protected DirectoryBeanBuilder<E, P> builder;

    public E getById(long id)
    {
        return (E) manager.find(beanClass, id);
    }

    /**
     * Loads all updated entries from directory
     *
     * @param date date to update from
     */
    public void syncAll(Date date) throws DatatypeConfigurationException
    {
        List<P> updated = getAllUpdatedDirectoryBeans(date);

        for (P proxy : updated)
        {
            builder.setProxy(proxy);
            E bean = builder.buildEntity();

            manager.merge(bean);
        }
    }

    /**
     * Loads and updates entries which exist locally
     *
     * @param date date to update from
     */
    public abstract void sync(Date date) throws DatatypeConfigurationException;

    /**
     *
     * Loads entry from directory and creates its local copy
     *
     * @param id
     * @return
     */
    public E syncOne(Long id)
    {
        P proxy = getDirectoryBean(id);

        builder.setProxy(proxy);
        E bean = builder.buildEntity();
        if (bean == null)
        {
            return null;
        }

        return manager.merge(bean);
    }

    /**
     *
     * @param request
     * @return list of entities
     */
    public List<E> loadFromDirectory(R request)
    {
        List<P> ps = getDirectoryBeansList(request);
        List<E> result = new ArrayList<E>();
        for (P proxy : ps)
        {
            builder.setProxy(proxy);
            E bean = builder.buildEntity();

            //TODO: Is it good to create list of managed objects?
            result.add(manager.merge(bean));
        }

        return result;
    }

    public E loadFromDirectory(Long id)
    {
        P proxy = getDirectoryBean(id);

        builder.setProxy(proxy);
        E bean = builder.buildEntity();

        return manager.merge(bean);
    }

    public abstract List<P> getAllUpdatedDirectoryBeans(Date date) throws DatatypeConfigurationException;

    public abstract P getDirectoryBean(Long id);

    public abstract List<P> getDirectoryBeansList(R r);
}
