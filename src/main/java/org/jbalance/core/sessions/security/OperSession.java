package org.jbalance.core.sessions.security;

import org.jbalance.core.exception.JBalanceException;
import org.jbalance.core.model.security.Oper;
import org.jbalance.core.sessions.GenericSession;

public interface OperSession<T extends Oper> extends GenericSession<T>{

    public T edit(T bean, boolean changePassoword) throws JBalanceException;

    public T getByLogin(String Value);

    public String hashPassword(String pass);

    public boolean validatePassword(String given, String expected);

}
