package org.jbalance.core.sessions.security;

import javax.ejb.Stateless;
import org.jbalance.core.model.security.SystemObject;
import org.jbalance.core.sessions.GenericSessionBean;

@Stateless
public class SystemObjectSessionBean extends GenericSessionBean<SystemObject> implements SystemObjectSession {

    @Override
    public Class<SystemObject> getClazz() {
        return SystemObject.class;
    }

}
