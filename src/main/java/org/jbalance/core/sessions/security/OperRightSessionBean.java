package org.jbalance.core.sessions.security;

import javax.ejb.Stateless;
import org.jbalance.core.model.security.OperRight;
import org.jbalance.core.sessions.GenericSessionBean;

@Stateless
public class OperRightSessionBean extends GenericSessionBean<OperRight> implements OperRightSession {

    @Override
    public Class<OperRight> getClazz() {
        return OperRight.class;
    }
}
