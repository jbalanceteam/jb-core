package org.jbalance.core.sessions.security;

import java.security.Principal;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.NoResultException;
import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.LoginException;
import org.jbalance.core.model.security.Oper;
import org.jbalance.core.utils.Log;
import org.jboss.security.SimplePrincipal;
import org.jboss.security.auth.spi.DatabaseServerLoginModule;

/**
 *
 * @author roman
 */
public abstract class BaseJbalanceLoginModule extends DatabaseServerLoginModule {

    protected Log log = new Log(getClass());

    private OperSession op;

    @Override
    public void initialize(Subject subject, CallbackHandler callbackHandler, Map<String, ?> sharedState, Map<String, ?> options) {
        super.initialize(subject, callbackHandler, sharedState, options);
    }

    @Override
    protected String getUsersPassword() throws LoginException {
        Oper o;
        log.info("getting user password");

        try {
            o = getOperSession().getByLogin(getUsername());

        } catch (NoResultException e) {

            log.warn("SECURITY LOGIN MODULE: oper not found");
            throw new LoginException();
        } catch (Exception e) {

            log.warn("SECURITY LOGIN MODULE: exception:" + e.getMessage());
            throw new LoginException();

        }

        if (o == null) {
            throw new LoginException();
        }

        return o.getPasswordHash();
    }

    @Override
    protected boolean validatePassword(String inputPassword, String expectedPassword) {

        log.info("validating user password");
        try {
            return getOperSession().validatePassword(inputPassword, expectedPassword);
        } catch (Exception ex) {
            Logger.getLogger(BaseJbalanceLoginModule.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    protected String createPasswordHash(String username, String password, String digestOption) throws LoginException {

        log.info("hashing user password");
        try {
            return getOperSession().hashPassword(password);
        } catch (Exception ex) {
            Logger.getLogger(BaseJbalanceLoginModule.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    @Override
    protected Principal createIdentity(String username) throws Exception {

        log.info("creating indentity, username is " + username);
        return new SimplePrincipal(username);
    }

    public abstract OperSession getOperSession() throws Exception;
}
