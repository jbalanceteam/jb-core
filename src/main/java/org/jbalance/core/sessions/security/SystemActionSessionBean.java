package org.jbalance.core.sessions.security;

import javax.ejb.Stateless;
import org.jbalance.core.model.security.SystemAction;
import org.jbalance.core.sessions.GenericSessionBean;

@Stateless
public class SystemActionSessionBean extends GenericSessionBean<SystemAction> implements SystemActionSession {

    @Override
    public Class<SystemAction> getClazz() {
        return SystemAction.class;
    }

}
