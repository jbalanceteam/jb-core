
package org.jbalance.core.sessions.security;

import org.jbalance.core.model.security.OperRight;
import org.jbalance.core.sessions.GenericSession;

/**
 *
 * @author roman
 */
public interface OperRightSession  extends GenericSession<OperRight>{
}
