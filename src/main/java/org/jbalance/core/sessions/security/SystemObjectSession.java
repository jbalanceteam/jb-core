package org.jbalance.core.sessions.security;

import org.jbalance.core.model.security.SystemObject;
import org.jbalance.core.sessions.GenericSession;

/**
 *
 * @author roman
 */
public interface SystemObjectSession extends GenericSession<SystemObject> {

}
