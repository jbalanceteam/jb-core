package org.jbalance.core.filter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class StringFilter extends Filter<StringFilterType, String> {
    private static final long serialVersionUID = 1L;

    private StringFilterType filterType;
    private List<String> filterValues;

    public StringFilter() {}

    public StringFilter(StringFilterType filterType, List<String> filterValues) {
        setFilterType(filterType);
        setFilterValues(filterValues);
    }

    public StringFilter(StringFilterType filterType, String... filterValues) {
        setFilterType(filterType);
        setFilterValues(filterValues);
    }

    public StringFilter(String filterValue) {
        setFilterType(StringFilterType.EQUALS);
        setFilterValue(filterValue);
    }

    /**
     * Convenience constructor that sets the filterType to {@link StringFilterType#IN}.
     * @param filterValues
     */
    public StringFilter(List<String> filterValues) {
        setFilterType(StringFilterType.IN);
        setFilterValues(filterValues);
    }
    
    @XmlAttribute(required=true)
    @Override
    public StringFilterType getFilterType() {
        return filterType;
    }

    @Override
    public void setFilterType(StringFilterType filterType) {
        this.filterType = filterType;
    }

    @XmlElement(name="filterValue")
    @Override
    public List<String> getFilterValues() {
        if (filterValues == null) {
            filterValues = new ArrayList<String>();
        }
        return filterValues;
    }

    @Override
    public void setFilterValues(List<String> filterValues) {
        this.filterValues = filterValues;
    }

    public void setFilterValues(String... filterValues) {
        setFilterValues(Arrays.asList(filterValues));
    }
}
