package org.jbalance.core.filter;

import java.util.List;

public interface FilterType {

    public String getDescription();
    public ArgumentsExpected getArgumentsExpected();
    public abstract void appendFilter(StringBuilder query, FilterParams params, String fieldName, List<?> filterValues);

    public enum ArgumentsExpected {
        NONE,
        ONE,
        TWO,
        ONE_OR_MORE;
    }
}
