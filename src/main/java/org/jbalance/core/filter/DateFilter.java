package org.jbalance.core.filter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class DateFilter extends Filter<DateFilterType, Date> {
    private static final long serialVersionUID = 1L;

    private DateFilterType filterType;
    private List<Date> filterValues;

    public DateFilter() {}

    public DateFilter(DateFilterType filterType, Date filterValue) {
        setFilterType(filterType);
        setFilterValue(filterValue);
    }

    public DateFilter(DateFilterType filterType, List<Date> filterValues) {
        setFilterType(filterType);
        setFilterValues(filterValues);
    }

    public DateFilter(DateFilterType filterType) {
        setFilterType(filterType);
    }

    @XmlAttribute(required=true)
    @Override
    public DateFilterType getFilterType() {
        return filterType;
    }

    @Override
    public void setFilterType(DateFilterType filterType) {
        this.filterType = filterType;
    }

    @XmlElement(name="filterValue")
    @Override
    public List<Date> getFilterValues() {
        if (filterValues == null) {
            filterValues = new ArrayList<Date>();
        }
        return filterValues;
    }

    @Override
    public void setFilterValues(List<Date> filterValues) {
        this.filterValues = filterValues;
    }
}
