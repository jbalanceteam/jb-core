package org.jbalance.core.filter;

import java.util.List;

import org.jbalance.core.utils.Obj;
import org.jbalance.core.utils.Str;

public enum StringFilterType implements FilterType {
    /**
     * <p>Equals or equal-to.  Equivalent to:</p>
     *
     * <p><tt><i>lvalue</i> = <i>rvalue</i></tt></p>
     */
    EQUALS("Equals", ArgumentsExpected.ONE),
    /**
     * <p>Starts with.  Equivalent to:</p>
     *
     * <p><tt><i>lvalue</i> LIKE '<i>rvalue</i>%'</tt></p>
     */
    STARTS_WITH("Starts With", ArgumentsExpected.ONE),
    /**
     * <p>Ends with.  Equivalent to:</p>
     *
     * <p><tt><i>lvalue</i> LIKE '%<i>rvalue</i>'</tt></p>
     */
    ENDS_WITH("Ends With", ArgumentsExpected.ONE),
    /**
     * <p>Contains.  Equivalent to:</p>
     *
     * <p><tt><i>lvalue</i> LIKE '%<i>rvalue</i>%'</tt></p>
     */
    CONTAINS("Contains", ArgumentsExpected.ONE),
    /**
     * <p>Is null.  Equivalent to:</p>
     *
     * <p><tt><i>lvalue</i> IS NULL}.</tt></p>
     */
    NULL("Is Null", ArgumentsExpected.NONE),
    /**
     * <p>Is null.  Equivalent to:</p>
     *
     * <p><tt><i>lvalue</i> IS NULL or <i>length(trim(lvalue)) = 0</i>}.</tt></p>
     */
    BLANK("Is Null or Blank", ArgumentsExpected.NONE),
    /**
     * <p>In.  Equivalent to:</p>
     *
     * <p><tt><i>lvalue</i> IN (<i>rvalue</i>[0],
     * <i>rvalue</i>[1], ...)</tt></p>
     */
    IN("In", ArgumentsExpected.ONE_OR_MORE),
    /**
     * <p>Between.  Equivalent to:</p>
     *
     * <p><tt><i>lvalue</i> BETWEEN <i>rvalue1</i> AND <i>rvalue2</i></tt> <i>(inclusive)</i></p>
     */
    BETWEEN("Between", ArgumentsExpected.TWO);

    private final String description;
    private final ArgumentsExpected argumentsExpected;

    /**
     * Returns a user-friendly description of the comparison operator
     *
     * @return a user-friendly description of the comparison operator
     */
    @Override
    public String getDescription() {
        return description;
    }

    /**
     * Returns the number of arguments expected by this filter type
     *
     * @return number of arguments expected by this filter type
     */
    public ArgumentsExpected getArgumentsExpected() {
        return argumentsExpected;
    }

    /**
     * Creates a new comparison with the specified description (internal use
     * only)
     *
     * @param description A user-friendly description of the comparison
     * operator
     */
    private StringFilterType(String description, ArgumentsExpected argumentsExpected) {
        this.description = description;
        this.argumentsExpected = argumentsExpected;
    }

    /**
     * Returns the user-friendly description of the comparison.  This returns
     * the same value as {@link #getDescription()}
     *
     * @return the user-friendly description of the comparison
     *
     * @see #getDescription()
     */
    @Override
    public String toString() {
        return description;
    }

    @Override
    public void appendFilter(StringBuilder query, FilterParams params, String fieldName, List<?> filterValues) {
        if(query == null) {
            throw new IllegalArgumentException("Query string builder is required");
        }

        if(params == null) {
            throw new IllegalArgumentException("Filter parameters are required");
        }

        if(Str.empty(fieldName)) {
            throw new IllegalArgumentException("Field name is required");
        }

        String paramName = null;
        if(this == NULL) {
            query.append(" ");
            query.append(fieldName);
            query.append(" is null");
        } else if (this == BLANK) {
            query.append(" (");
            query.append(fieldName);
            query.append(" is null");
            query.append(" or length(trim(");
            query.append(fieldName);
            query.append(")) = 0)");
        } else if(this == IN) {
            if(filterValues == null || filterValues.isEmpty()) {
                // IN (empty list) will always evaluate to false, but we will
                // get a query syntax exception if the parameter list is
                // missing.  To avoid this, substitute with an expression that
                // will always evaluate to false.
                query.append(" 1=0");
            } else {
                paramName = params.addParam(filterValues);
                query.append(" ");
                query.append(fieldName);
                query.append(" in (:");
                query.append(paramName);
                query.append(")");
            }
        } else if(this == BETWEEN) {
            if(filterValues == null || filterValues.size() != 2) {
                throw new IllegalArgumentException(this + " filter type requires exactly two filter values");
            }
            String paramName1 = params.addParam(filterValues.get(0));
            String paramName2 = params.addParam(filterValues.get(1));

            query.append(" ");
            query.append(fieldName);
            query.append(" between ");
            query.append(" :").append(paramName1);
            query.append(" and :").append(paramName2);
        } else {
            if(filterValues == null || filterValues.size() != 1) {
                throw new IllegalArgumentException(this + " filter type requires exactly one filter value");
            }

            String paramValue = Obj.coerceToString(filterValues.get(0));

            query.append(" ");
            query.append(fieldName);
            query.append(" ");

            switch(this) {
                case CONTAINS:
                    paramName = params.addParam('%' + paramValue + '%');
                    query.append(" like :");
                    query.append(paramName);
                    break;
                case ENDS_WITH:
                    paramName = params.addParam('%' + paramValue);
                    query.append(" like :");
                    query.append(paramName);
                    break;
                case EQUALS:
                    paramName = params.addParam(paramValue);
                    query.append(" = :");
                    query.append(paramName);
                    break;
                case STARTS_WITH:
                    paramName = params.addParam(paramValue + '%');
                    query.append(" like :");
                    query.append(paramName);
                    break;
                default:
                    // Should never happen, but Sonar will complain if we don't
                    // have a default case
                    throw new UnsupportedOperationException("Unsupported filter type: " + this);
            }
        }
    }
}
