package org.jbalance.core.wsclient;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

/**
 *
 * @author Komov Roman <komov.r@gmail.com>
 */
public class BaseDirectoryClient
{

    protected URL wsdlLocation;

    public URL getWsdlLocation(String name)
    {
        Properties prop = new Properties();
        InputStream input = null;

        try
        {

            String filename = "wsdllocation.properties";
            input = BaseDirectoryClient.class.getClassLoader().getResourceAsStream(filename);
            if (input == null)
            {
                return null;
            }

            prop.load(input);

            String urlString = prop.getProperty(name);

            if (urlString != null)
            {
                return new URL(urlString);
            }

        } catch (IOException ex)
        {
            ex.printStackTrace();
        } finally
        {
            if (input != null)
            {
                try
                {
                    input.close();
                } catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }
}
