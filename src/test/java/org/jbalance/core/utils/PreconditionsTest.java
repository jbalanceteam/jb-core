package org.jbalance.core.utils;

import javax.persistence.EntityManager;

import org.jbalance.core.exception.JBalanceException;
import org.jbalance.core.model.security.Oper;
import org.junit.Test;
import org.mockito.Mockito;

import com.google.common.collect.Sets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

public class PreconditionsTest {

    @Test(expected = IllegalArgumentException.class)
    public void checkNotNull() {
        Object o = null;
        Preconditions.checkNotNull(o);
    }

    @Test
    public void checkNotNullWithMessage() {
        Object o = null;
        String message = "message";
        try {
            Preconditions.checkNotNull(o, message);
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), message);
        }
    }

    @Test
    public void checkNotNullWithMessageAndParameters() {
        Object o = null;
        String message = "message";
        String messageWithParam = message+"%s";
        String param = "param";
        try {
            Preconditions.checkNotNull(o, messageWithParam, param);
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), message+param);
        }
    }

    @Test
    public void checkNotNullSuccess() {
        Object o = new Object();
        Object result = Preconditions.checkNotNull(o);
        assertEquals(o, result);
    }

    @Test
    public void checkNotNullWithMessageSuccess() {
        Object o = new Object();
        String message = "message";
        Object result = Preconditions.checkNotNull(o, message);
        assertEquals(o, result);
    }

    @Test
    public void checkNotNullWithMessageAndParametersSuccess() {
        Object o = new Object();
        String message = "message%s";
        String param = "param";
        Object result = Preconditions.checkNotNull(o, message, param);
        assertEquals(o, result);
    }

    @Test
    public void checkIds2WithNoPrimaryKey() throws JBalanceException {
        EntityManager em = Mockito.mock(EntityManager.class);
        try {
            Preconditions.checkIds2(em, Oper.class, Sets.newHashSet(), "test");
        } catch (IllegalArgumentException e) {
            assertFalse(e.getMessage().contains("{"));
            return;
        }
        fail("JBalanceException was expected.");
    }
}
