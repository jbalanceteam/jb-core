package org.jbalance.core.utils;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author roman
 */
public class CollectionsTest {

    @Test
    public void testMap() {
        class X {

            private String test1;
            private Long test2;

            public X(String test1, Long test2) {
                this.test1 = test1;
                this.test2 = test2;
            }

            public String getTest1() {
                return test1;
            }

            public void setTest1(String test1) {
                this.test1 = test1;
            }

            public Long getTest2() {
                return test2;
            }

            public void setTest2(Long test2) {
                this.test2 = test2;
            }

        }

        List<X> list = new LinkedList<X>();

        list.add(new X("test", (long) 1));
        list.add(new X("test2", (long) 15));
        list.add(new X("test", (long) 15));
        list.add(new X("test2", (long) 15));

        try {
            Map<String, List<X>> r = CollectionUtil.map(list, "test1",String.class);
            
            Assert.assertTrue(r.size()==2);
            Assert.assertTrue(r.containsKey("test"));
            Assert.assertTrue(r.get("test").size()==2);
            Assert.assertTrue(r.containsKey("test2"));
            Assert.assertTrue(r.get("test2").size()==2);
            
        } catch (Exception e) {
            Assert.assertFalse(true);
        }

    }

}
