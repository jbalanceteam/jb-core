package org.jbalance.core.sessions;

import javax.inject.Inject;


import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public abstract class BaseTest {

    @Deployment
    public static WebArchive createDeployment() {
        WebArchive archive = ShrinkWrap.create(WebArchive.class, "test.war")
                //	            .addClass(Roles.class)
                .addPackages(true, "org.jbalance.core","org.jbalance.directory")
                //	            .addPackages(true, "com.google")
                //	            .addClass(RolesTest.class)
                //	            http://blog.ringerc.id.au/2012/06/jbas011440-cant-find-persistence-unit.html
                .addAsWebInfResource("META-INF/persistence.xml", "classes/META-INF/persistence.xml")
                .addAsWebInfResource("META-INF/jboss-deployment-structure.xml", "jboss-deployment-structure.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
        System.err.println("===============");
        archive.writeTo(System.err, org.jboss.shrinkwrap.api.formatter.Formatters.VERBOSE);
//	      System.out.println(archive.getContent());

        return archive;
    }

}
